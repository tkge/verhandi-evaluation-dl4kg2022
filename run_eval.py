import json

import grpc
import csv
from typing import List, Tuple, Dict

import generated.link_prediction_pb2 as pb2
import generated.link_prediction_pb2_grpc as pb2_grpc

from configparser import ConfigParser

CONFIG = ConfigParser()
CONFIG.read("config.ini")


def main() -> None:
    triples = load_dataset()
    ha1, ha10, mrr = eval(triples)
    print(f"Hits@1: {ha1}, Hits@10: {ha10}, MRR: {mrr}")


def load_dataset() -> List[Tuple[int, int, int, str]]:
    triples = []

    with open(CONFIG.get("dataset", "entity2id"), "r") as e2id, open(
        CONFIG.get("dataset", "relation2id"), "r"
    ) as r2id:
        e2id_csv = csv.reader(e2id, delimiter="\t")
        r2id_csv = csv.reader(r2id, delimiter="\t")
        entity2id = {row[0]: int(row[1]) for row in e2id_csv}
        relation2id = {row[0]: int(row[1]) for row in r2id_csv}

    with open(CONFIG.get("dataset", "test_split"), "r") as ts:
        ts_csv = csv.reader(ts, delimiter="\t")
        for row in ts_csv:
            head = entity2id[row[0]]
            relation = relation2id[row[1]]
            tail = entity2id[row[2]]
            time = row[3]
            triples.append((head, relation, tail, time))

    return triples


def eval(positive_triples: List[Tuple[int, int, int, str]]) -> Tuple[float, float, float]:
    num_facts = len(positive_triples)
    num_hits_at_1 = 0
    num_hits_at_10 = 0
    rrs = []
    index = 0

    with open("servers.json") as sj_file:
        server_json = json.load(sj_file)

    print('Starting evaluation')

    num_entities = int(CONFIG.get("dataset", "num_entities"))
    for head, relation, tail, time in positive_triples:
        dataset = CONFIG.get("dataset", "name")
        time = f"{time[8:10]}-{time[5:7]}-{time[:4]}"
        triple = head, relation, tail, time

        phead = predict_link(-1, relation, tail, time, num_entities, dataset, server_json)
        ptail = predict_link(head, relation, -1, time, num_entities, dataset, server_json)

        # hits@1
        ph, pt = phead.facts[0], ptail.facts[0]
        if triple == (ph.head, ph.relation, ph.tail, ph.time):
            num_hits_at_1 += 1
        if triple == (pt.head, pt.relation, pt.tail, pt.time):
            num_hits_at_1 += 1

        # hits@10
        pheads = [(t.head, t.relation, t.tail, t.time) for t in phead.facts]
        ptails = [(t.head, t.relation, t.tail, t.time) for t in ptail.facts]
        if triple in pheads[:10]:
            num_hits_at_10 += 1
        if triple in ptails[:10]:
            num_hits_at_10 += 1

        # mrr
        try:
            rank_phead = pheads.index(triple)
            rr_phead = 1 / (rank_phead + 1)
        except ValueError:
            rr_phead = 0
        try:
            rank_ptail = ptails.index(triple)
            rr_ptail = 1 / (rank_ptail + 1)
        except ValueError:
            rr_ptail = 0
        rrs.extend((rr_phead, rr_ptail))

        index += 1
        print(f'Fact: {index}/{num_facts}')

    hits_at_1 = num_hits_at_1 / (num_facts * 2)
    hits_at_10 = num_hits_at_10 / (num_facts * 2)
    mrr = 1 / len(rrs) * (sum(rrs))

    return hits_at_1, hits_at_10, mrr


def predict_link(
        head: int,
        relation: int,
        tail: int,
        time: str,
        num_replies: int,
        dataset: str,
        servers: List[Dict[str, str]]
) -> pb2.LinkPredictionResponse:
    host = servers[0]['host']
    port = servers[0]['port']
    channel = grpc.insecure_channel(f"{host}:{port}")
    stub = pb2_grpc.LinkPredictionStub(channel=channel)
    query = pb2.LinkPredictionQuery(
        head=head,
        relation=relation,
        tail=tail,
        time=time,
        num_replies=num_replies,
        dataset=dataset,
    )
    response = stub.PredictLink(query)

    return response


if __name__ == "__main__":
    main()
