import subprocess
import time
import json
from typing import List, Dict


def main() -> None:
    print('WARNING!! \nMake sure all three tkge servers are running!')

    weight1 = [1.0]
    weight2 = [1.0]
    weight3 = [1.0]

    ports = [50051, 50052, 50053]

    for i, _ in enumerate(weight1):
        weights = [weight1[i], weight2[i], weight3[i]]
        update_weights(ports, weights)
        p = subprocess.Popen(['python', 'run_weighted_ensemble_eval.py'])
        while p.poll() is None:
            time.sleep(300)
        p.kill()


def update_weights(ports: List[int], weights: List[float]):
    servers = []
    for i, port in enumerate(ports):
        server = {'host': 'localhost',
                  'port': port,
                  'weight': weights[i]}
        servers.append(server)

    with open('servers.json', 'w') as sj_file:
        json.dump(servers, sj_file)


if __name__ == '__main__':
    main()
