import grpc
import csv
import json
from typing import List, Tuple, Dict
from bayes_opt import BayesianOptimization
from bayes_opt.logger import JSONLogger
from bayes_opt.event import Events
from bayes_opt.util import load_logs

import generated.link_prediction_pb2 as pb2
import generated.link_prediction_pb2_grpc as pb2_grpc

from configparser import ConfigParser

CONFIG = ConfigParser()
CONFIG.read("config.ini")


def run_ensemble_optim(w1: float, w2: float, w3: float) -> float:
    triples = load_dataset()
    print(f'w1: {w1}, w2: {w2}, w3: {w3}')
    weights = [w1, w2, w3]
    ha1, ha10, mrr = eval(triples, weights)

    with open("servers.json") as sj_file:
        servers = json.load(sj_file)

    s1 = f"DE_SimplE: {w1}, DE_DistMult: {w2}, DE_TransE: {w3}"
    s2 = f'Hits@1: {ha1}, Hits@10: {ha10}, MRR: {mrr}'
    print(s2)
    with open('results.txt', 'a') as file:
        file.write(s1 + '\n')
        file.write(s2 + '\n')

    return mrr


def load_dataset() -> List[Tuple[int, int, int, str]]:
    triples = []

    with open(CONFIG.get("dataset", "entity2id"), "r") as e2id, open(
        CONFIG.get("dataset", "relation2id"), "r"
    ) as r2id:
        e2id_csv = csv.reader(e2id, delimiter="\t")
        r2id_csv = csv.reader(r2id, delimiter="\t")
        entity2id = {row[0]: int(row[1]) for row in e2id_csv}
        relation2id = {row[0]: int(row[1]) for row in r2id_csv}

    with open(CONFIG.get("dataset", "test_split"), "r") as ts:
        ts_csv = csv.reader(ts, delimiter="\t")
        for row in ts_csv:
            head = entity2id[row[0]]
            relation = relation2id[row[1]]
            tail = entity2id[row[2]]
            time = row[3]
            triples.append((head, relation, tail, time))

    return triples


def eval(positive_triples: List[Tuple[int, int, int, str]], weights: List[float]
         ) -> Tuple[float, float, float]:
    num_facts = len(positive_triples)
    num_hits_at_1 = 0
    num_hits_at_10 = 0
    rrs = []
    index = 0

    with open("servers.json") as sj_file:
        server_json = json.load(sj_file)

    print('Starting evaluation')

    num_entities = int(CONFIG.get("dataset", "num_entities"))
    for head, relation, tail, time in positive_triples:
        dataset = CONFIG.get("dataset", "name")
        time = f"{time[8:10]}-{time[5:7]}-{time[:4]}"
        triple = head, relation, tail, time

        phead = predict_link(-1, relation, tail, time, num_entities, dataset, server_json, weights)
        ptail = predict_link(head, relation, -1, time, num_entities, dataset, server_json, weights)

        phead_dict: Dict[Tuple[int, int, int, str], int] = {}
        ptail_dict: Dict[Tuple[int, int, int, str], int] = {}

        for responses in phead:
            response_facts, weight = responses
            for i, fact in enumerate(response_facts.facts):
                factt = (fact.head, fact.relation, fact.tail, fact.time)
                old_score = phead_dict.get(factt, 0)
                phead_dict[factt] = old_score + ((len(response_facts.facts) - i) * float(weight))

        for responses in ptail:
            response_facts, weight = responses
            for i, fact in enumerate(response_facts.facts):
                factt = (fact.head, fact.relation, fact.tail, fact.time)
                old_score = ptail_dict.get(factt, 0)
                ptail_dict[factt] = old_score + ((len(response_facts.facts) - i) * float(weight))

        phead_rank_list = [
            (pb2.Fact(head=k[0], relation=k[1], tail=k[2], time=k[3], score=0), v)
            for k, v in phead_dict.items()
        ]
        ptail_rank_list = [
            (pb2.Fact(head=k[0], relation=k[1], tail=k[2], time=k[3], score=0), v)
            for k, v in ptail_dict.items()
        ]

        phead_sorted = sorted(phead_rank_list, key=lambda x: x[1], reverse=True)
        ptail_sorted = sorted(ptail_rank_list, key=lambda x: x[1], reverse=True)

        # hits@1
        ph, pt = phead_sorted[0][0], ptail_sorted[0][0]
        if triple == (ph.head, ph.relation, ph.tail, ph.time):
            num_hits_at_1 += 1
        if triple == (pt.head, pt.relation, pt.tail, pt.time):
            num_hits_at_1 += 1

        # hits@10
        pheads = [(t[0].head, t[0].relation, t[0].tail, t[0].time) for t in phead_sorted]
        ptails = [(t[0].head, t[0].relation, t[0].tail, t[0].time) for t in ptail_sorted]
        if triple in pheads[:10]:
            num_hits_at_10 += 1
        if triple in ptails[:10]:
            num_hits_at_10 += 1

        # mrr
        try:
            rank_phead = pheads.index(triple)
            rr_phead = 1 / (rank_phead + 1)
        except ValueError:
            rr_phead = 0
        try:
            rank_ptail = ptails.index(triple)
            rr_ptail = 1 / (rank_ptail + 1)
        except ValueError:
            rr_ptail = 0
        rrs.extend((rr_phead, rr_ptail))

        index += 1
        print(f'Fact: {index}/{num_facts}')

    hits_at_1 = num_hits_at_1 / (num_facts * 2)
    hits_at_10 = num_hits_at_10 / (num_facts * 2)
    mrr = 1 / len(rrs) * (sum(rrs))

    return hits_at_1, hits_at_10, mrr


def predict_link(
        head: int,
        relation: int,
        tail: int,
        time: str,
        num_replies: int,
        dataset: str,
        servers: List[Dict[str, str]],
        weights: List[float]
) -> pb2.LinkPredictionResponse:
    channels = [
        (grpc.insecure_channel(f"{server['host']}:{server['port']}"), weights[i])
        for i, server in enumerate(servers)
    ]
    stubs = [(pb2_grpc.LinkPredictionStub(channel=channel), weight) for channel, weight in channels]
    query = pb2.LinkPredictionQuery(
        head=head,
        relation=relation,
        tail=tail,
        time=time,
        num_replies=num_replies,
        dataset=dataset,
    )
    responses = [(stub.PredictLink(query), weight) for stub, weight in stubs]

    return responses


if __name__ == "__main__":
    bounds = {'w1': (0.8, 1), 'w2': (0.5, 1), 'w3': (0.0, 0.5)}

    optimizer = BayesianOptimization(f=run_ensemble_optim, pbounds=bounds)

    optimizer = load_logs(optimizer, logs=["./logs.json"])

    optimizer.maximize(init_points=5, n_iter=10)

    print(f'Max found: {optimizer.max}')
    for i, res in enumerate(optimizer.res):
        print(f'Iteration {i}:\n\t{res}')
